import React ,{Component} from 'react';
import ImageLinkForm from './components/ImageLinkForm/ImageLinkForm'
import Navigation from './components/Navigation/Navigation'
import Logo from './components/Logo/Logo'
import Rank from './components/Rank/Rank'
import './App.css';
import Particles from 'react-particles-js';
import Clarifai from 'clarifai';
import FaceRecognition from './components/FaceRecognition/FaceRecognition'

const app = new Clarifai.App({
// 03900824c4284045ade05b3bd1b2e92c
apiKey: '472edf88ea0b4f69987fe2575aaeb1b0'

})

const particlesOptions = {
particles: {
 number : {
value : 30,
density:{

  enable:true,
  value_area : 800
}
 }
 }
}



class App extends Component {

  constructor(){

    super();
    this.state={

      input: '',
      imageUrl: '',
      box :{}
     // prediction : {},
    }

  }



  calculateFaceLocation =(response)=>
  {

const clarifaiFace = response.outputs[0].data.regions[0].region_info.bounding_box;
console.log('clarifaiface', clarifaiFace);

const image = document.getElementById('input_image');
const width = Number(image.width);
const height = Number(image.height);

return {

  leftCol : clarifaiFace.left_col * width,
  topRow : clarifaiFace.top_row * height,
  rightCol : width - (clarifaiFace.right_col * width),
  bottomRow : height - (clarifaiFace.bottom_row * height)
}


  }

    onInputChange = (event) => {
    
    this.setState({input: event.target.value})
  }

  displayFaceBox = (box)=>{
    this.setState({box : box});

    // var myimage = document.getElementById('input_image');
    // var ctx = myimage.getContext("2d");

    // ctx.beginPath();
    // ctx.lineWidth= "6"
    // ctx.strokeStyle="red";
    // console.log('leftcol',box['leftCol'])
    // ctx.rect(box['leftCol'],box['topRow'],(box['rightCol']-box['leftCol']) ,
    //   (box['topRow']-box['bottomRow']))
    // ctx.stroke();


  }

  onSubmit = (imageUrl) =>{
      console.log('clicked');

      this.setState({imageUrl:this.state.input })

      app.models.initModel({id: Clarifai.FACE_DETECT_MODEL})
      .then(generalModel => {
        //this.setState({prediction : generalModel.predict(this.state.input)});
        return generalModel.predict(this.state.input)
      })
      .then(response =>

        //console.log(response.outputs[0].data.regions[0].region_info.bounding_box);
        this.displayFaceBox(
          this.calculateFaceLocation(response))


    // var concepts = response['outputs'][0]['data']['concepts']
      ).catch(err => console.log(err));

      
  }

  render(){
  return (
    <div className="App">
                <Particles className='particles'
                params={particlesOptions}
                    />

    <Navigation />
    <Logo />
    <Rank />
    <ImageLinkForm onInputChange = {this.onInputChange} onSubmit= {this.onSubmit}/>
  
    
  <FaceRecognition displayImage= {this.state.imageUrl} box = {this.state.box} />
     
    </div>
  );
}
}
export default App;
